#!/usr/bin/env sh

set -o errexit
set -o nounset
set -o pipefail

function _sh {
    local command=$1
    echo -e "\e[1;34m$(pwd)\e[32m\$ ${command}\e[39m"
    sh -c "${command}"
}

# Update group www-data
_sh "groupmod -g ${BUILD_ARG_APP_USER_ID} www-data"

# Add user www-data
_sh "adduser -h /home/www-data -u ${BUILD_ARG_APP_USER_ID} -G www-data -s /bin/false -D www-data"

# Post install
_sh "rm $0"
